@extends('layouts.admin')


@section('content')
@if(Session::has('updated_category'))
<p class="alert alert-success">{{session('updated_category')}}</p>

    

    
@else
<p class="alert alert-success">{{session('deleted_category')}}</p>
    
@endif
    





<h1>Categories</h1>
<div class="col-sm-6">
        {!! Form::open(['method' => 'POST','action' => 'AdminCategoriesController@store', 'files'=>true]) !!}

        <div class="form-group">
            {!! Form::label('name', 'Name:') !!}
            {!! Form::text('name', null, ['class'=>'form-control']) !!}
        </div>
        
       

        <div class="form-group">
                {!! Form::submit('Create Category', ['class'=>'btn btn-primary']) !!}
            
            </div>
            
            
            {!! Form::close() !!}
            
            @include('includes.form_error')
            
            
            
       
   


</div>

<div class="col-sm-6">
        @if ($categories)
        
    
        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Created Date</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($categories as $category )
                    
                
                <tr>
                <td>{{$category->id}}</td>
                    <td><a href="{{route('admin.categories.edit',$category->id)}}">  {{$category->name}}</a></td>
                <td>{{$category->created_at? $category->created_at->diffForHumans() : 'Unspecified date'}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    
            @endif  


</div>

    
@endsection