@extends('layouts.admin')



@section('content')

@if(Session::has('deleted_post'))
<p class="alert alert-success">{{session('deleted_post')}}</p>

@endif

<h1>Posts</h1>

<table class="table table-hover">
    <thead>
      <tr>
        <th>#</th>
        <th>Photo</th>
        <th>Owner</th>
        <th>Category</th>
        
        <th>Title</th>
        <th>Body</th>
        
        <th>Created_at</th>
        <th>Updated_at</th>
      </tr>
    </thead>
    <tbody>
        @if($posts)
        @foreach($posts as $post)
      <tr>
      <td>{{$post->id}}</td>
      <td><img height="60" src="{{$post->photo ? $post->photo->file : 'https://via.placeholder.com/400/#000

        C/O https://placeholder.com/'}} " alt=""></td>
      <td><a href="{{route('admin.posts.edit', $post->id)}}">{{$post->user->name}}</a></td>
      <td>{{$post->category? $post->category->name : 'Uncategorized'}}</td>
     
      <td>{{$post->title}}</td>
      <td>{{ str_limit($post->body), 20}}</td>
      
        <td>{{$post->created_at->diffForHumans()}}</td>
        <td>{{$post->updated_at->diffForHumans()}}</td>
      </tr>
      @endforeach
      @endif
      
    </tbody>
  </table>



@stop